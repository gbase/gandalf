(function () {

  angular.module('App')
    .controller('WizOneStepOneCTRL', [WizOneStepOneCTRL]);

  function WizOneStepOneCTRL() {

    var vm = this;

    vm.onCreateTwo = function (model) {

      console.log('Created an instance of resource Two:', model);

    };

  }

})();
