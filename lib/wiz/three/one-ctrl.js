(function () {

  require('../../com/gstv/gbase/gandalf/index.js');

  angular.module('App')
    .controller('WizThreeStepOneCTRL', ['gandalf', WizThreeStepOneCTRL]);

  function WizThreeStepOneCTRL(gandalf) {

    var vm = this;

    // Skip the second step.
    gandalf.getWizardSteps()[ 1 ].skip = true;

  }

})();
