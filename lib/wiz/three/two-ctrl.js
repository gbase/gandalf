(function () {

  require('../../com/gstv/gbase/gandalf/index.js');

  angular.module('App')
    .controller('WizThreeStepTwoCTRL', ['gandalf', WizThreeStepTwoCTRL]);

  function WizThreeStepTwoCTRL(gandalf) {

    var vm = this;

    // Lock the navigation.
    gandalf.locked = true;

  }

})();
