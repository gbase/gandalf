(function () {

  angular.module('App')
    .controller('WizTwoStepOneCTRL', [WizTwoStepOneCTRL]);

  function WizTwoStepOneCTRL() {

    var vm = this;

    vm.onCreateThree = function (model) {

      console.log('Created an instance of resource Three:', model);

    };

  }

})();
