(function () {

  var CONSTANTS = require('./constants.js');

  /**
   * @ngdoc function
   * @module com.gstv.gbase.gandalf
   * @name WizardData
   * @id com.gstv.gbase.gandalf.WizardData
   * @class
   * @description
   * A class used to hold wizard instance data.
   * @example ```new WizardData( ... )```
   * @param {string} name The name of the wizard.
   * @param {int} id The id of the wizard model resource.
   * @param {array.<com.gstv.gbase.gandalf.WizardStepStructure>} steps The the list of step objects for the wizard. See {@link com.gstv.gbase.gandalf.WizardStepStructure `WizardStepStructure`}.
   * @param {object} resource The wizard resource.
   * @param {object} options The wizard options.
   * @param {class} modelClass (Optional) The class to be used when instantiating the `model`.
   * @param {class} instanceData (Optional) An object containing data used to configure the specific instance of the wizard.
   *
   * */

  angular.module(require('./name.js').module)
    .constant('WizardData', WizardData);

  function WizardData(name, id, steps, resource, options, modelClass, instanceData) {

    var self = this;

    // *** Values ***

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardData
     * @name name
     * @returns {string} The name and url of the wizard.
     * */
    this.name = name;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardData
     * @name id
     * @returns {int} The unique identifier of the RESTful resource represented by the wizard.
     * */
    this.id = id;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardData
     * @name steps
     * @returns {array.<com.gstv.gbase.gandalf.WizardStepStructure>} The the list of step objects for the wizard. See {@link com.gstv.gbase.gandalf.WizardStepStructure `WizardStepStructure`}.
     * */
    this.steps = angular.copy(steps);

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardData
     * @name currentStepIndex
     * @returns {int} The index of the current step.
     * */
    this.currentStepIndex = 0;


    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardData
     * @name resource
     * @returns {object} The RESTful resource.
     * */
    this.resource = resource;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardData
     * @name options
     * @returns {object} The options structure.
     * */
    this.options = options;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardData
     * @name modelInitialized
     * @returns {boolean} A flag designating whether or not the model has been initialized.
     * */
    this.modelInitialized = false;

    var defaultModel = id === CONSTANTS.DEFAULT_ID ? {} : {id: id};

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardData
     * @name model
     * @returns {object} The data being edited and the source of data binding for all wizard step views.
     * */
    this.model = modelClass ? ( !resource || id === CONSTANTS.DEFAULT_ID || 'undefined' === typeof id ? new modelClass(defaultModel) : {} ) : {};

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardData
     * @name instanceData
     * @returns {object} An object containing data used to configure the specific instance of the wizard.
     * */
    this.instanceData = instanceData;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardData
     * @name original
     * @returns {object|undefined} The original model if this is a copy, otherwise `undefined`.
     * */
    this.original = undefined;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardData
     * @name saved
     * @returns {boolean} A flag designating whether or not the model has been saved/submitted.
     * */
    this.saved = false;

    // *** Nesting Support ***

    /** @type boolean */
    this.isChild = false;

    /** @type function(model {object}) */
    this.reopenHandler = undefined;

    this.promises = [];

    this.callPromises = function (failed) {

      for (var i = 0; i < self.promises.length; i++) {

        var promise = self.promises[i];

        var callback = failed ? promise.reject : promise.resolve;

        callback();

      }

      self.promises = [];

    };

  };

})();
