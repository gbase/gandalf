(function () {

  /**
   * @ngdoc interface
   * @module com.gstv.gbase.gandalf
   * @name WizardStepStructure
   * @id com.gstv.gbase.gandalf.WizardStepStructure
   * @description Wizard Step Structure
   * */

  /**
   * @ngdoc property
   * @propertyOf com.gstv.gbase.gandalf.WizardStepStructure
   * @returns {string} The step portion of a wizard url for the target step.
   * @name url
   * */

  /**
   * @ngdoc property
   * @propertyOf com.gstv.gbase.gandalf.WizardStepStructure
   * @returns {string} The template to load for the target step.
   * @name templateUrl
   * */

  /**
   * @ngdoc property
   * @propertyOf com.gstv.gbase.gandalf.WizardStepStructure
   * @returns {boolean} A flag designating whether or not to skip the target step.
   * @name skip
   * */

  /**
   * @ngdoc function
   * @module com.gstv.gbase.gandalf
   * @name WizardState
   * @id com.gstv.gbase.gandalf.WizardState
   * @class
   * @description
   * A class used to hold wizard state related data.
   * Used as a state object passed to the Angular UI Router `$stateProvider.state()` method.
   * @example ```new WizardState( ... )```
   *
   * @param {string} name The name of the wizard.
   * @param {array.<com.gstv.gbase.gandalf.WizardStepStructure>} steps The the list of step objects for the wizard. See {@link com.gstv.gbase.gandalf.WizardStepStructure `WizardStepStructure`}.
   * @param {function|array} enterHandler The state enter handler for the wizard state. Used by GANDALF internally.
   * @param {string} resourceFactoryName The name of the resource factory to inject into each wizard instance. A resource must implement the {@link com.gstv.gbase.gandalf.WizardResourceAPI `WizardResourceAPI`} interface.
   * @param {string} optionsFactoryName The name of the options factory to inject into each wizard instance.
   * @param {function|array} closeHandler The close handler to be called when the wizard is closed. This function or array is injectable and can receive the closed {@link com.gstv.gbase.gandalf.WizardData `WizardData`} instance by injecting `GandalfExitData`.
   * @param {class} modelClass (Optional) The class to be used when instantiating the `model` for each wizard instance.
   * @param {function|array} setup (Optional) A function used to run some setup procedures when a wizard is first instantiated. Called after the `model` is initialized. This function or array is injectable and can receive the current {@link com.gstv.gbase.gandalf.WizardData `WizardData`} instance by injecting `GandalfWizardData`.
   *
   * */

  angular.module(require('./name.js').module)
    .constant('WizardState', WizardState);

  function WizardState(name, steps, enterHandler, resourceFactoryName, optionsFactoryName, closeHandler, modelClass, setup) {

    // *** Values ***

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardState
     * @name wizardName
     * @returns {string} The name and url of the wizard.
     * */
    this.wizardName = name;


    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardState
     * @name steps
     * @returns {array.<com.gstv.gbase.gandalf.WizardStepStructure>} The wizard steps. See {@link com.gstv.gbase.gandalf.WizardStepStructure `WizardStepStructure`}.
     * */
    this.steps = steps;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardState
     * @name resourceFactoryName
     * @returns {string} The name of the resource factory.
     * */
    this.resourceFactoryName = resourceFactoryName;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardState
     * @name optionsFactoryName
     * @returns {string} The name of the options factory.
     * */
    this.optionsFactoryName = optionsFactoryName;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardState
     * @name closeHandler
     * @returns {function(com.gstv.gbase.gandalf.WizardData)} The close handler for the wizard.
     * */
    this.closeHandler = closeHandler;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardState
     * @name modelClass
     * @returns {class} The class to be used when instantiating the `model` for each wizard instance.
     * */
    this.modelClass = modelClass;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardState
     * @name setup
     * @returns {function|array} (Optional) A function used to run some setup procedures when a wizard is first instantiated. Called after the `model` is initialized. This function or array is injectable and can receive the current {@link com.gstv.gbase.gandalf.WizardData `WizardData`} instance by injecting `GandalfWizardData`.
     * */
    this.setup = setup;


    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardState
     * @name firstStep
     * @returns {string} The name/url of the first step.
     * */
    this.firstStep = steps[0].url || 1;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardState
     * @name stepMap
     * @returns {object} An object with step url keys and step object values.
     * */
    this.stepMap = WizardState.getStepMap(steps);

    // *** UI Router Values ***

    /** @type string */
    this.url = '/' + name + '/{id}/{step}';

    /** @type object */
    this.params = {
      id: {value: WizardState.DEFAULT_ID},
      step: {value: this.firstStep},
      data: this.data || {}
    };

    // For use in the `templateUrl` function.
    var thisInstance = this;

    /** @type string */
    this.templateUrl = function (stateParams) {

      return thisInstance.stepMap[stateParams.step || thisInstance.firstStep].templateUrl;

    };

    /** @type function|array */
    this.onEnter = enterHandler;

  };

  var CONSTANTS = require('./constants.js');

  WizardState.DEFAULT_ID = CONSTANTS.DEFAULT_ID;
  WizardState.COPY_ID_PREFIX = CONSTANTS.COPY_ID_PREFIX;

  WizardState.getStepMap = function (steps) {

    var stepMap = {};

    for (var k in steps) {

      var step = steps[k];
      step.index = parseInt(k);

      var stepUrl = step.url || parseInt(k) + 1;

      stepMap[stepUrl] = step;

    }

    return stepMap;

  };

})();
