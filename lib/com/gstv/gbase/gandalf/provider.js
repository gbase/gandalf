(function () {

  require('./wizard-data.js');
  require('./wizard-state.js');

  /**
   * @ngdoc object
   * @module com.gstv.gbase.gandalf
   * @name gandalfProvider
   * @id com.gstv.gbase.gandalf.gandalfProvider
   * @description
   * The provider for the gandalf service.
   * <br /><br />
   * This provider exposes the following methods:
   * <br />
   * 1. `getWizardState` - Returns a `WizardState` object suitable for use with Angular UI Router's `$stateProvider.state()` method.
   *
   * */

  angular.module(require('./name.js').module)
    .provider('gandalf', ['WizardState', 'WizardData', gandalf]);

  function gandalf(WizardState, WizardData) {

    // *** Wizard State Values ***

    var WIZARD_STATES = {};
    var CURRENT_WIZARD = undefined;
    var WIZARD_QUEUE = [];
    var WIZARD_STATE_RESOLVED = true;

    return {

      /**
       * @ngdoc service
       * @module com.gstv.gbase.gandalf
       * @name gandalf
       * @id com.gstv.gbase.gandalf.gandalf
       * @description
       * The `gandalf` service is the primary mechanism for interacting with GANDALF wizards
       * and exposes an API for navigation and resource management.
       *
       * */

      $get: [

        '$injector', '$state', '$q',
        function ($injector, $state, $q) {

          var BASE_STATE_OPTIONS = {inherit: false};

          var getNewWizardData = function (wizardState, id, modelSeed, copy, instanceData) {

            var resource = wizardState.resourceFactoryName ? $injector.get(wizardState.resourceFactoryName) : undefined;
            var options = wizardState.optionsFactoryName ? $injector.get(wizardState.optionsFactoryName) : undefined;

            var wizardData = new WizardData(
              wizardState.wizardName,
              copy ? WizardState.DEFAULT_ID : id,
              wizardState.steps,
              resource,
              options,
              wizardState.modelClass,
              instanceData
            );

            wizardData.model = wizardData.model || {};

            // Apply the model seed values.
            if ('object' === typeof modelSeed) {
              var modelSeedCopy = angular.copy(modelSeed);
              if ('undefined' !== typeof wizardState.modelClass) {
                wizardData.model = new wizardState.modelClass(modelSeedCopy);
              } else {
                for (var k in modelSeedCopy) {
                  wizardData.model[k] = modelSeedCopy[k];
                }
              }
            }

            function triggerSetup() {
              if ('undefined' !== typeof wizardState.setup) {
                $injector.invoke(wizardState.setup, wizardState.setup, {
                  // IMPORTANT: Make `GandalfWizardData` available for injection.
                  GandalfWizardData: wizardData
                });
              }
            }

            if (resource && 'function' === typeof resource.get && !isNaN(id)) {

              // IMPORTANT: Update transaction status.
              GANDALF.accessingResource = true;

              resource.get(id).then(function (data) {

                // IMPORTANT: Update transaction status.
                GANDALF.accessingResource = false;

                wizardData.id = copy ? WizardState.DEFAULT_ID : wizardData.id;
                wizardData.model = data;

                if (copy && 'object' === typeof wizardData.model) {
                  // IMPORTANT: Remove the `id` from a copied `model`.
                  delete wizardData.model.id;
                }

                wizardData.original = copy ? angular.copy(data) : undefined;
                wizardData.modelInitialized = true;

                triggerSetup();

                wizardData.callPromises();

              }, function () {

                // IMPORTANT: Update transaction status.
                GANDALF.accessingResource = false;

                triggerSetup();

                wizardData.callPromises(true);

              });

            } else if (isNaN(id)) {

              wizardData.modelInitialized = true;

              triggerSetup();

            } else {

              triggerSetup();

            }

            return wizardData;

          };

          var setCurrentWizard = function (wizardName, id, child, modelSeed, copy, instanceData) {

            if ('undefined' !== typeof CURRENT_WIZARD) {

              var namesMatch = String(CURRENT_WIZARD.name) === String(wizardName);
              var idsMatch = ( isNaN(CURRENT_WIZARD.id) && isNaN(id) ) || ( String(CURRENT_WIZARD.id) === String(id) );

              if (namesMatch && idsMatch && !child) {
                // IMPORTANT: Return if this is NOT a New Wizard!
                return;
              } else if (!child) {
                // IMPORTANT: New wizard, not a child and CURRENT_WIZARD exists, reset.
                reset();
              }

              // INFO: If we are trying to create a child wizard then we
              // will queue the current wizard and generate new wizard data
              // as the current wizard.

              if (child) WIZARD_QUEUE.push(CURRENT_WIZARD);

            }

            CURRENT_WIZARD = getNewWizardData(WIZARD_STATES[wizardName], id, modelSeed, copy, instanceData);
            CURRENT_WIZARD.isChild = child ? true : false;

          };

          var closeCurrentWizard = function () {

            var childWizard = CURRENT_WIZARD;

            CURRENT_WIZARD = WIZARD_QUEUE.length ? WIZARD_QUEUE.pop() : undefined;

            // Navigate to the popped wizard!
            if (CURRENT_WIZARD && 'function' === typeof CURRENT_WIZARD.reopenHandler) {

              CURRENT_WIZARD.reopenHandler(childWizard.model, childWizard);

              CURRENT_WIZARD.reopenHandler = undefined;

            } else {

              var wizardState = WIZARD_STATES[childWizard.name];

              if (wizardState && 'function' === typeof wizardState.closeHandler) {

                $injector.invoke(wizardState.closeHandler, wizardState.closeHandler, {
                  // IMPORTANT: Make `GandalfExitData` available for injection.
                  GandalfExitData: childWizard
                });

              }

            }

            // Change state.
            if (CURRENT_WIZARD) {

              WIZARD_STATE_RESOLVED = false;

              var resolveWizardState = function () {

                WIZARD_STATE_RESOLVED = true;

              };

              $state.go(WIZARD_STATES[CURRENT_WIZARD.name], {
                id: CURRENT_WIZARD.id,
                step: CURRENT_WIZARD.steps[CURRENT_WIZARD.currentStepIndex].url
              }, BASE_STATE_OPTIONS).then(resolveWizardState, resolveWizardState);

            }

          };

          var optionsQueued = 0;

          function updateAccessingOptions(value) {
            optionsQueued += value ? 1 : -1;
            GANDALF.accessingOptions = optionsQueued < 1 ? false : true;
          }

          function reset() {
            optionsQueued = 0;
            CURRENT_WIZARD = undefined;
            WIZARD_QUEUE = [];
            WIZARD_STATE_RESOLVED = true;
          }

          var GANDALF = {

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name getWizardData
             * @return {com.gstv.gbase.gandalf.WizardData} The current wizard data object.
             * */

            getWizardData: function () {

              return CURRENT_WIZARD;

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name getWizardName
             * @return {string} The name of the current wizard.
             * */

            getWizardName: function () {

              return CURRENT_WIZARD ? CURRENT_WIZARD.name : undefined;

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name getWizardSteps
             * @return {array.<com.gstv.gbase.gandalf.WizardStepStructure>} An instance of the list of step objects for the current wizard. See {@link com.gstv.gbase.gandalf.WizardStepStructure `WizardStepStructure`}.
             * */

            getWizardSteps: function () {

              return CURRENT_WIZARD ? CURRENT_WIZARD.steps : undefined;

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name getStepIndex
             * @param {int} offset An offset from the current step index.
             * @return {int} The current step index of the current wizard based on the supplied `offset` and taking into account any steps with `skip` set to `true`.
             * */

            getStepIndex: function (offset) {

              if (!CURRENT_WIZARD) {
                return -1;
              }

              var newIndex = CURRENT_WIZARD.currentStepIndex + parseInt(offset);

              if (newIndex > CURRENT_WIZARD.steps.length - 1) newIndex = CURRENT_WIZARD.steps.length - 1;
              if (newIndex < 0) newIndex = 0;

              // *** Skip Logic ***
              var step = CURRENT_WIZARD.steps[newIndex];
              if (

                step.skip &&
                ( offset >= 0 || newIndex !== 0 ) &&
                ( offset < 0 || newIndex !== CURRENT_WIZARD.steps.length - 1 )

              ) {

                var skipOffset = offset < 0 ? offset - 1 : offset + 1;

                newIndex = GANDALF.getStepIndex(skipOffset);

              }

              return newIndex;

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name getStepUrlByIndex
             * @param {int} index The index of the target step.
             * @return {string|int|undefined} The url of the step at the supplied `index`.
             * */

            getStepUrlByIndex: function (index) {

              if (!CURRENT_WIZARD) {
                return undefined;
              }

              var step = CURRENT_WIZARD.steps[index];

              return step.url || parseInt(index) + 1;

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name getModel
             * @return {object} The resource or model instance for the current wizard.
             * */

            getModel: function () {

              return CURRENT_WIZARD ? CURRENT_WIZARD.model : undefined;

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name getCreateMode
             * @return {boolean} A flag designating whether or not the current wizard resource is new.
             * */

            getCreateMode: function () {

              return CURRENT_WIZARD ? isNaN(CURRENT_WIZARD.id) : true;

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name getModelInitialized
             * @return {boolean|promise} `false` if there is no longer an available wizard state, otherwise a promise that will be resolved when the model has been initialized.
             * */

            getModelInitialized: function () {

              if (!CURRENT_WIZARD) {
                return false;
              }

              var deferred = $q.defer();

              CURRENT_WIZARD.promises.push(deferred);

              if (CURRENT_WIZARD.modelInitialized) {

                deferred.resolve();

              }

              return deferred.promise;

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name getOptions
             * @return {object} The options instance for the current wizard.
             * */

            getOptions: function () {

              return CURRENT_WIZARD ? CURRENT_WIZARD.options : undefined;

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name getInstanceData
             * @return {object} The instance data for the current wizard instance.
             * */

            getInstanceData: function () {

              return CURRENT_WIZARD ? CURRENT_WIZARD.instanceData : undefined;

            },

            /**
             * @ngdoc property
             * @propertyOf com.gstv.gbase.gandalf.gandalf
             * @name locked
             * @returns {boolean} A flag designating whether or not navigation is locked. Set to `true` to lock the navigation for GANDALF.
             * */

            locked: false,

            /**
             * @ngdoc property
             * @propertyOf com.gstv.gbase.gandalf.gandalf
             * @name accessingResource
             * @returns {boolean} A flag designating whether or not a resource request is currently active.
             * */

            accessingResource: false,

            /**
             * @ngdoc property
             * @propertyOf com.gstv.gbase.gandalf.gandalf
             * @name accessingOptions
             * @returns {boolean} A flag designating whether or not an options request is currently active.
             * */

            accessingOptions: false,

            // *** Navigate Internal ***

            navigateInternal: function (wizardName, itemId, stepUrl, parentReopenHandler, bypassState, modelSeed, overrideLock, instanceData) {

              // TRICKY: IMPORTANT: Do NOT navigate if the wizard state is not resolved.
              if (!WIZARD_STATE_RESOLVED) return;

              // TRICKY: You can never bypass setting the state when opening a child wizard.
              if (parentReopenHandler && bypassState) return;

              itemId = 'undefined' === typeof itemId ? WizardState.DEFAULT_ID : itemId;

              var copy = false;

              if ('string' === typeof itemId && itemId.indexOf(WizardState.COPY_ID_PREFIX) === 0) {

                copy = true;

                var newItemId = itemId.substr(WizardState.COPY_ID_PREFIX.length, itemId.length - WizardState.COPY_ID_PREFIX.length);

                itemId = parseFloat(newItemId);

              }

              // Navigation locking.
              if (GANDALF.locked && !overrideLock) return;

              var wizardState = WIZARD_STATES[wizardName];

              // Don't navigate if there is no wizard state with the supplied name.
              if (!wizardState) return;

              var isChild = 'function' === typeof parentReopenHandler;

              var stateParams = {
                id: copy ? WizardState.DEFAULT_ID : itemId,
                step: stepUrl
              };

              if (isChild) {
                if (CURRENT_WIZARD) {
                  CURRENT_WIZARD.reopenHandler = parentReopenHandler;
                } else {
                  reset();
                }
              }

              // Set the current wizard data object.
              setCurrentWizard(wizardName, itemId, isChild, modelSeed, copy, instanceData);

              // Set the current step index.
              CURRENT_WIZARD.currentStepIndex = wizardState.stepMap[stepUrl || wizardState.firstStep].index;

              var newState = bypassState ? undefined : $state.go(wizardState, stateParams, BASE_STATE_OPTIONS);

              // TRICKY: If `newState` is a promise, set the wizard when it is resolved.
              if (newState && 'function' === typeof newState.then) {

                WIZARD_STATE_RESOLVED = false;

                newState.then(function () {

                  WIZARD_STATE_RESOLVED = true;

                }, function () {

                  WIZARD_STATE_RESOLVED = true;

                });

              }

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name navigate
             * @description
             * Navigate to a specified wizard and step.
             *
             * @param {string} wizardName (Required) The name of the target wizard.
             * @param {int} itemId The unique identifier of the resource to target.
             * Use `create` to create a new resource. Use `copy:{id}` to copy a resource, for example: `copy:44`.
             * @param {string|int} stepUrl The url or name of the target step.
             * @param {function(object, WizardData)} parentReopenHandler The function on a parent wizard that will be called with the `model` and `WizardData` of the nested child wizard when the child is closed and the parent is re-opened.
             * @param {object} modelSeed An object used to seed initial values on the model. Only used when creating a resource.
             * @param {object} instanceData An object containing data used to configure the specific instance of the target wizard.
             *
             * @returns {undefined} undefined
             * */

            navigate: function (wizardName, itemId, stepUrl, parentReopenHandler, modelSeed, instanceData) {

              GANDALF.navigateInternal(wizardName, itemId, stepUrl, parentReopenHandler, undefined, modelSeed, undefined, instanceData);

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name newStepByOffset
             * @description
             * Navigate to a step in the current wizard based on the supplied `offset` and taking into account any steps with `skip` set to `true`.
             * @param {int} offset The offset from the current step index to the target step index.
             * @returns {undefined} undefined
             * */

            newStepByOffset: function (offset, overrideLock) {

              if (!CURRENT_WIZARD) {
                return;
              }

              var newIndex = GANDALF.getStepIndex(offset);
              var newStepUrl = GANDALF.getStepUrlByIndex(newIndex);

              GANDALF.navigateInternal(CURRENT_WIZARD.name, $state.params.id, newStepUrl, undefined, undefined, undefined, overrideLock);

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name previousStep
             * @description
             * Navigate backwards one step in the current wizard taking into account any steps with `skip` set to `true`.
             * @returns {undefined} undefined
             * */

            previousStep: function () {

              var overrideLock = false;

              // TRICKY: If...
              if (
                // there is a current wizard and..
              CURRENT_WIZARD &&
                // it is in "create" mode and ...
              isNaN(CURRENT_WIZARD.id) &&
                // it is not a copy then...
              'undefined' === typeof CURRENT_WIZARD.original
              ) {

                // allow navigation to the previous step.
                overrideLock = true;

              }

              GANDALF.newStepByOffset(-1, overrideLock);

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name nextStep
             * @description
             * Navigate forwards one step in the current wizard taking into account any steps with `skip` set to `true`.
             * @returns {undefined} undefined
             * */

            nextStep: function () {

              GANDALF.newStepByOffset(1);

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name saveWizard
             * @description
             * Save or create the `model` resource for the current wizard.
             * @param {function(...object)} onSuccess A callback function to be called when the resource promise is resolved.
             * @param {function(...object)} onError A callback function to be called if the resource promise is rejected.
             * @returns {undefined} undefined
             * */

            saveWizard: function (onSuccess, onError) {

              if (!CURRENT_WIZARD) {
                if ('function' === typeof onError) {
                  onError({
                    message: 'ERROR: No current wizard resource available.'
                  });
                }
                return;
              }

              if (!CURRENT_WIZARD.resource && 'function' === typeof onSuccess) {
                CURRENT_WIZARD.saved = true;
                onSuccess();
                return;
              }

              // Interact with the RESTful Resource.
              if (CURRENT_WIZARD.model && CURRENT_WIZARD.resource) {

                var model = CURRENT_WIZARD.model;
                var resource = CURRENT_WIZARD.resource;
                var savedWizard = CURRENT_WIZARD;

                var thenOk = function (data) {

                  savedWizard.saved = true;

                  // IMPORTANT: Update transaction status.
                  GANDALF.accessingResource = false;

                  if ('function' === typeof onSuccess) {

                    onSuccess.apply(this, arguments);

                  }

                };

                var thenFail = function () {

                  // IMPORTANT: Update transaction status.
                  GANDALF.accessingResource = false;

                  if ('function' === typeof onError) {

                    onError.apply(this, arguments);

                  }

                };

                // IMPORTANT: Update transaction status.
                GANDALF.accessingResource = true;

                if (model.id) {

                  // Save
                  if ('function' === typeof resource.save) {

                    resource.save(model).then(thenOk, thenFail);

                  }

                } else {

                  // Create
                  if ('function' === typeof resource.create) {

                    resource.create(model).then(thenOk, thenFail);

                  }

                }

              }

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name closeWizard
             * @description
             * Close the current wizard and re-open the last wizard in the queue if one is present otherwise call the `closeHandler` for the current wizard passing the current instance of `WizardData`.
             * See {@link com.gstv.gbase.gandalf.WizardData `WizardData`}.
             * */

            closeWizard: function () {

              closeCurrentWizard();

            },

            /**
             * @ngdoc method
             * @methodOf com.gstv.gbase.gandalf.gandalf
             * @name queueOptions
             * @description
             * Notify `gandalf` that options are being loaded.
             * @param {array.<promise>} optionPromises An array of pending promise objects used to toggle the value of `accessingOptions`.
             * @return {promise} A promise that will resolve when all option promises have been resolved.
             * */

            queueOptions: function (optionPromises) {

              optionPromises = optionPromises || [];

              updateAccessingOptions(true);

              return $q.all(optionPromises).then(function () {
                updateAccessingOptions(false);
              }, function () {
                // Change option loading status even when there is an error.
                updateAccessingOptions(false);
              });

            }

          };

          return GANDALF;

        }

      ],

      /**
       * @ngdoc interface
       * @module com.gstv.gbase.gandalf
       * @name WizardResourceAPI
       * @id com.gstv.gbase.gandalf.WizardResourceAPI
       * */

      /**
       * @ngdoc method
       * @methodOf com.gstv.gbase.gandalf.WizardResourceAPI
       * @name get
       * @description A method for retrieving a resource by the supplied `id`.
       * @param {int} id The RESTful resource unique identifier.
       * @returns {promise} A promise object.
       * */

      /**
       * @ngdoc method
       * @methodOf com.gstv.gbase.gandalf.WizardResourceAPI
       * @name create
       * @description A method to create a resource from the supplied `data`.
       * @param {object} data The data representing the resource to be created.
       * @returns {promise} A promise object.
       * */

      /**
       * @ngdoc method
       * @methodOf com.gstv.gbase.gandalf.WizardResourceAPI
       * @name save
       * @description A method to save an existing resource based on the supplied `data`.
       * @param {object} data The resource object to be saved.
       * @returns {promise} A promise object.
       * */

      /**
       * @ngdoc method
       * @name getWizardState
       * @methodOf com.gstv.gbase.gandalf.gandalfProvider
       * @description
       * Creates a new Wizard in the GANDALF system.
       * <br />
       * ## URL INFO:
       * ### The url for a wizard consists of the following parts:
       * 1. The parent state's url.
       * 1. The `wizardName`.
       * 1. The resource `id` or `'create'`. (Optional unless `step` is specified.)
       * 1. The step `url` if specified for the target step or the 1-based index of the step. (Optional)
       *
       * ### Examples: (Parent State URL: `/parent-state`, Wizard Name: `magic-wizard`, Resource ID: `'create'` or `44`, Step URL: `step-two`)
       * 1. `/parent-state/magic-wizard`
       * 1. `/parent-state/magic-wizard/create`
       * 1. `/parent-state/magic-wizard/44`
       * 1. `/parent-state/magic-wizard/create/step-two`
       * 1. `/parent-state/magic-wizard/44/step-two`
       *
       * @param {string} name (Required) The name of the wizard and the wizard portion of the state url (`...parent-state-url/wizard-name/{id}/{step}`). Must be unique.
       * @param {array.<com.gstv.gbase.gandalf.WizardStepStructure>} steps (Required) The list of step objects for the wizard. See {@link com.gstv.gbase.gandalf.WizardStepStructure `WizardStepStructure`}.
       * @param {string} resourceFactoryName The name of the Restangular resource to be injected into the instance of `WizardData` (See {@link com.gstv.gbase.gandalf.WizardData `WizardData`}) when a wizard state is activated. A resource must implement the {@link com.gstv.gbase.gandalf.WizardResourceAPI `WizardResourceAPI`} interface.
       * @param {string} optionsFactoryName The name of an options object structure to be injected into a new `WizardData` instance.
       * @param {function|array} closeHandler The close handler to be called when closing the wizard state if it is not a "child" wizard. This function or array is injectable and can receive the closed {@link com.gstv.gbase.gandalf.WizardData `WizardData`} instance by injecting `GandalfExitData`.
       * @param {class} modelClass (Optional) The class to be used when instantiating the `model` for each wizard instance.
       * @param {function|array} setup (Optional) A function used to run some setup procedures when a wizard is first instantiated. Called after the `model` is initialized. This function or array is injectable and can receive the current {@link com.gstv.gbase.gandalf.WizardData `WizardData`} instance by injecting `GandalfWizardData`.
       *
       * @returns {com.gstv.gbase.gandalf.WizardState} A `WizardState` object to be passed to the Angular UI Router `$stateProvider.state()` method in a module config function.
       *
       * */

      getWizardState: function (name, steps, resourceFactoryName, optionsFactoryName, closeHandler, modelClass, setup) {

        var onEnter = [

          'gandalf', '$stateParams',
          function (gandalf, $stateParams) {

            // Update queue/nesting.
            gandalf.navigateInternal(name, $stateParams.id, $stateParams.step, undefined, true);

          }

        ];

        var wizardState = new WizardState(
          name,
          steps,
          onEnter,
          resourceFactoryName,
          optionsFactoryName,
          closeHandler,
          modelClass,
          setup
        );

        WIZARD_STATES[name] = wizardState;

        return wizardState;

      }

    };

  }

})();
