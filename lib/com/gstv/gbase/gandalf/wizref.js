(function () {

  require('./wizard-state.js');

  /**
   * @ngdoc directive
   * @module com.gstv.gbase.gandalf
   * @name wizref
   * @id com.gstv.gbase.gandalf.wizref
   * @description
   * Used just like `ui-sref` from Angular UI Router except it always sets `ui-sref-opts` to `{inherit:false}`
   * and sets the `id` parameter to it's default if it is not specified.
   * `id` can also be set to `create` to create a new resource or `copy:{id}` to copy a resource, for example: `copy:44`.
   * @example
   * ```<a wizref="main.wizard1({id:44,step:'step-two'})">Wizard 1, ID 44, Step 2</a>```
   * <br />OR<br />
   * ```<a wizref="main.wizard1({id:'create',step:'step-two'})">Wizard 1, ID 44, Step 2</a>```
   * <br />OR<br />
   * ```<a wizref="main.wizard1({id:'copy:44',step:'step-two'})">Wizard 1, ID 44, Step 2</a>```
   * */

  angular.module(require('./name.js').module)
    .directive('wizref', ['WizardState', '$compile', '$parse', wizref]);

  function wizref(WizardState, $compile, $parse) {

    function parseStateRef(ref) {
      var parsed = ref.match(/^([^(]+?)\s*(\((.*)\))?$/);
      if (!parsed || parsed.length !== 4) return undefined;
      return { state: parsed[1], params: ($parse(parsed[3]))({}) };
    }

    return {

      restrict: 'A',
      compile: function (tElement, tAttrs, transclude) {

        // Get the value of `wizref` and transpose it to `ui-sref`.
        if ('string' === typeof tAttrs.wizref) {

          var wizRef = tAttrs.wizref;

          var refParts = parseStateRef(wizRef);

          if (refParts && 'object' === typeof refParts.params) {

            // Set `id` to it's default.
            if ('undefined' === typeof refParts.params.id) {

              refParts.params.id = WizardState.DEFAULT_ID;

              wizRef = refParts.state + '(' + angular.toJson(refParts.params) + ')';

            }

          }

          tElement.attr('ui-sref', wizRef);

          // TRICKY: Always set the `inherit` value of `ui-sref-opts` to `false` so that wizard state params are reset to their defaults.
          tElement.attr('ui-sref-opts', '{inherit:false}');

          // TRICKY: Remove `wizref` to avoid infinite compilation.
          tElement.removeAttr('wizref');

        }

        return function (scope, element, attrs) {

          // IMPORTANT: Compile the element to trigger the new directives.
          $compile(element)(scope);

        }

      }

    };

  }

})();
