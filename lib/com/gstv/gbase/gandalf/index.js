(function () {

  require('jquery');
  require('angular');
  require('angular-ui-router');

  // *** Module Exports ***

  module.exports = require('./name.js');

  // *** Modules ***

  /**
   * @ngdoc overview
   * @name com.gstv.gbase.gandalf
   * @description
   * The primary module for the GANDALF framework.
   *
   * See the <a href="README.html" target="_blank">README</a>.
   *
   * */

  angular.module(module.exports.module, ['ui.router']);

  require('./ctrl.js');
  require('./wizref.js');

})();
