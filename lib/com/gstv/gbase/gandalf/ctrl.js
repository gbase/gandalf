(function () {

  require('./provider.js');

  /**
   * @ngdoc function
   * @module com.gstv.gbase.gandalf
   * @name WizardCTRL
   * @id com.gstv.gbase.gandalf.WizardCTRL
   * @description
   * The basic implementation of the GANDALF framework in a view.
   *
   * @param {angular.$scope} $scope The current Angular scope.
   * @param {com.gstv.gbase.gandalf.gandalf} gandalf The GANDALF service.
   *
   * @example ```<div ng-controller="WizardCTRL as vm"></div>```
   *
   * */

  angular.module(require('./name.js').module)
    .controller('WizardCTRL', ['$scope', 'gandalf', WizardCTRL]);

  function WizardCTRL($scope, gandalf) {

    // *** Internal Values ***

    var vm = this;

    // *** Values ***

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardCTRL
     * @name gandalf
     * @returns {com.gstv.gbase.gandalf.gandalf} A reference to the `gandalf` service, see: {@link com.gstv.gbase.gandalf.gandalf `gandalf`}.
     * */
    vm.gandalf = gandalf;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardCTRL
     * @name model
     * @returns {object} The current instance of the resource designated to be injected into the wizard via `gandalfProvider.getWizardState()`.
     * */
    vm.model = undefined;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardCTRL
     * @name options
     * @returns {object} The current instance of the options object designated to be injected into the wizard via `gandalfProvider.getWizardState()`.
     * */
    vm.options = undefined;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardCTRL
     * @name isCopy
     * @returns {boolean} A flag designating whether or not the current model is a copy.
     * */
    vm.isCopy = false;

    /**
     * @ngdoc property
     * @propertyOf com.gstv.gbase.gandalf.WizardCTRL
     * @name instanceData
     * @returns {object} The instance data for the current wizard instance.
     * */
    vm.instanceData = undefined;

    // *** Watch ***

    // Keep track of the model when the current wizard data changes.
    var unwatchModel = $scope.$watch(gandalf.getModel, function (newValue, oldValue) {

      vm.model = newValue;

      // Set `vm.isCopy`.
      var wizData = gandalf.getWizardData();
      if (wizData && wizData.original) {
        vm.isCopy = true;
      } else {
        vm.isCopy = false;
      }

    });

    // Keep track of the options when the current wizard data changes.
    var unwatchOptions = $scope.$watch(gandalf.getOptions, function (newValue, oldValue) {

      vm.options = newValue;

    });

    // Keep track of the instance data when the current wizard data changes.
    var unwatchInstanceData = $scope.$watch(gandalf.getInstanceData, function (newValue, oldValue) {

      vm.instanceData = newValue;

    });

    // *** Destroy ***

    $scope.$on('$destroy', function () {

      unwatchModel();
      unwatchOptions();
      unwatchInstanceData();

    });

  }

})();
