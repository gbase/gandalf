# GANDALF: Gbase Arbitrarily Navigated Document Action List Framework

> Simply put, GANDALF is a framework for creating "wizards" from a collection of UI views.

## Features:

1. Angular UI Router state configuration via the `gandalfProvider.getWizardState()` method.
1. RESTful resource management using injected Restangular resources.
1. "Per-wizard" form option injection.
1. Skip logic during typical next/previous navigation.
1. Navigation locking for preventing progression when data may be invalid or various other reasons.
1. Nested wizards to allow the creation of relationships between resources.

## How To Implement A Wizard:

1. Declare your wizard state and steps in your module config:
    ```
        angular.module( 'App', [ 'com.gstv.gbase.gandalf' ] )
            .config( function( gandalfProvider, $stateProvider ){
                var wizState = gandalfProvider.getWizardState( 'my-wiz', [
                    {
                        url: "step-one",
                        templateUrl: "wizards/my-wiz/step-one.html"
                    },
                    {
                        url: "step-two",
                        templateUrl: "wizards/my-wiz/step-two.html"
                    },
                    {
                        url: "step-three",
                        templateUrl: "wizards/my-wiz/step-three.html"
                    }
                ] );

                $stateProvider.state( 'wizards', {
                    url: '/wizards',
                    templateUrl: 'wizard-ui.html'
                } )
                .state( 'wizards.myWiz', wizState );
            } );
    ```
1. Create a wizard UI view (wizard-ui.html) to implement the `WizardCTRL` and bind the controls to the API in the exposed `gandalf` service:
    ```
        <div ng-controller="WizardCTRL as vm">
            Wizard
            <div><button value="Previous" ng-click="vm.gandalf.previousStep()" /><button value="Next" ng-click="vm.gandalf.nextStep()" /></div>
            <div><button value="Save" ng-click="vm.gandalf.saveWizard()" /><button value="Close" ng-click="vm.gandalf.closeWizard()" /></div>
            <div ui-view></div>
        </div>
    ```
1. Bind to the `model` in the `WizardCTRL` inside of your step view (wizards/my-wiz/step-one.html):
    ```
        <div>
            <input type="text" ng-model="vm.model.firstName" />
        </div>
    ```
1. Link to your wizard state from other application areas using the `wizref` directive just like you would use `ui-sref` in Angular UI Router:
    ```
        <a wizref="wizards.myWiz">Create something new</a><br />
        <a wizref="wizards.myWiz({id:55})">Edit something</a><br />
        <a wizref="wizards.myWiz({id:62,step:'step-three'})">Edit something starting at a specific step</a>
    ```
1. Tell GANDALF to skip a specific step from the view controller of another step:
    ```
        angular.module( 'App' )
            .controller( 'StepOneCTRL', function( gandalf ){
                var steps = gandalf.getWizardSteps();
                steps[ 1 ].skip = true;
            } );
    ```
1. Lock/unlock the navigation in GANDALF:
    ```
        vm.gandalf.locked = true;
        vm.gandalf.locked = false;
    ```

## Setup:

> Requires <a href="http://nodejs.org" target="_blank">nodejs</a> `0.11.x`.

1. `npm install -g bower`
1. `npm install -g gulp`
1. `npm install`
1. `bower install`

## Testing:

1. First time: `gulp test:e2e:withdriver`, after that: `gulp test:e2e:dev`

## Documentation Generation:

1. Generate documentation: `gulp docs:build` or live documentation mode: `gulp docs`
