require('jquery');
require('angular');
require('angular-ui-router');
require('./templates/index.js');

var gandalf = require('./lib/com/gstv/gbase/gandalf/index.js');

angular.module('App', [gandalf.module, 'ui.router', 'gstv.gbase.gandalf.templates'])
  .config([
    'gandalfProvider', '$stateProvider', '$locationProvider',
    function (gandalfProvider, $stateProvider, $locationProvider) {

      $locationProvider.html5Mode(true);

      var wiz1 = gandalfProvider.getWizardState('one', [
        {
          url: 'one',
          templateUrl: '/wiz/one/one.html'
        },
        {
          url: 'two',
          templateUrl: '/wiz/one/two.html'
        },
        {
          url: 'three',
          templateUrl: '/wiz/one/three.html'
        }
      ]);

      var wiz2 = gandalfProvider.getWizardState('two', [
        {
          url: 'one',
          templateUrl: '/wiz/two/one.html'
        },
        {
          url: 'two',
          templateUrl: '/wiz/two/two.html'
        },
        {
          url: 'three',
          templateUrl: '/wiz/two/three.html'
        }
      ]);

      var wiz3 = gandalfProvider.getWizardState('three', [
        {
          url: 'one',
          templateUrl: '/wiz/three/one.html'
        },
        {
          url: 'two',
          templateUrl: '/wiz/three/two.html'
        },
        {
          url: 'three',
          templateUrl: '/wiz/three/three.html'
        }
      ]);

      $stateProvider.state('wiz', {
        url: '/wiz',
        templateUrl: '/wiz/wiz-ui.html'
      })
        .state('wiz.one', wiz1)
        .state('wiz.two', wiz2)
        .state('wiz.three', wiz3);

    }
  ])
  .controller('AppCTRL', [
    '$scope', '$state', 'gandalf',
    function (scope, $state, gandalf) {

      return this;

    }
  ]);

require('./lib/wiz/one/one-ctrl.js');
require('./lib/wiz/two/one-ctrl.js');
require('./lib/wiz/three/one-ctrl.js');
require('./lib/wiz/three/two-ctrl.js');
