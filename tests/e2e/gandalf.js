describe('gandalf', function () {

  beforeEach(function (done) {

    browser.driver.get(baseUrl).then(function () {

      browser.sleep(1000);

      done();

    });

  });

  // *** WizRef ***

  it('should use "wizref" on an anchor to navigate to a wizard', function (done) {

    browser.findElement(by.css('#wizlink')).then(function (element) {

      element.click().then(function () {

        browser.sleep(1000);

        browser.getCurrentUrl().then(function (url) {

          url.should.endsWith('/wiz/one/create/one');

          done();

        });

      });

    });

  });

  // *** Default Step Navigation ***

  it('should display a default step when a step is not specified', function (done) {

    browser.driver.get(baseUrl + '/wiz/one').then(function () {

      browser.sleep(1000);

      browser.findElement(by.css('div[ui-view] div[ui-view]')).then(function (element) {

        element.getText().then(function (text) {

          text.should.startsWith('One One');

          done();

        });

      });

    });

  });

  // *** Next ***

  it('should go to the next step', function (done) {

    browser.driver.get(baseUrl + '/wiz/one').then(function () {

      browser.sleep(1000);

      browser.findElement(by.css('div[ui-view] button[ng-click="vm.gandalf.nextStep()"]')).then(function (element) {

        element.click().then(function () {

          browser.sleep(1000);

          browser.getCurrentUrl().then(function (url) {

            url.should.endsWith('/wiz/one/create/two');

            done();

          });

        });

      });

    });

  });

  // *** Previous ***

  it('should go to the previous step', function (done) {

    browser.driver.get(baseUrl + '/wiz/one/create/two').then(function () {

      browser.sleep(1000);

      browser.findElement(by.css('div[ui-view] button[ng-click="vm.gandalf.previousStep()"]')).then(function (element) {

        element.click().then(function () {

          browser.sleep(1000);

          browser.getCurrentUrl().then(function (url) {

            url.should.endsWith('/wiz/one/create/one');

            done();

          });

        });

      });

    });

  });

  // *** Child Wizard ***

  it('should open a child wizard', function (done) {

    browser.driver.get(baseUrl + '/wiz/one').then(function () {

      browser.sleep(1000);

      browser.findElement(by.css('div[ui-view] div[ui-view] button[ng-click="vm.gandalf.navigate(\'two\',\'create\',\'one\',ctrl.onCreateTwo)"]')).then(function (element) {

        element.click().then(function () {

          browser.sleep(1000);

          browser.getCurrentUrl().then(function (url) {

            url.should.endsWith('/wiz/two/create/one');

            done();

          });

        });

      });

    });

  });

  // *** Close Child Wizard ***

  it('should go back to a parent wizard when closing a child wizard', function (done) {

    browser.driver.get(baseUrl + '/wiz/one').then(function () {

      browser.sleep(1000);

      browser.findElement(by.css('div[ui-view] div[ui-view] button[ng-click="vm.gandalf.navigate(\'two\',\'create\',\'one\',ctrl.onCreateTwo)"]')).then(function (element) {

        element.click().then(function () {

          browser.sleep(1000);

          browser.getCurrentUrl().then(function (url) {

            // Open child.
            url.should.endsWith('/wiz/two/create/one');

            browser.findElement(by.css('div[ui-view] button[ng-click="vm.gandalf.closeWizard()"]')).then(function (element) {

              element.click().then(function () {

                browser.sleep(1000);

                browser.getCurrentUrl().then(function (url) {

                  // Back to parent.
                  url.should.endsWith('/wiz/one/create/one');

                  done();

                });

              });

            });

          });

        });

      });

    });

  });

  // *** Skip Logic ***

  it('should skip designated steps', function (done) {

    browser.driver.get(baseUrl + '/wiz/three/create/one').then(function () {

      browser.sleep(1000);

      browser.findElement(by.css('div[ui-view] button[ng-click="vm.gandalf.nextStep()"]')).then(function (element) {

        element.click().then(function () {

          browser.sleep(1000);

          browser.getCurrentUrl().then(function (url) {

            url.should.endsWith('/wiz/three/create/three');

            done();

          });

        });

      });

    });

  });

  // *** Navigation Locking ***

  it('should lock the navigation', function (done) {

    browser.driver.get(baseUrl + '/wiz/three/create/two').then(function () {

      browser.sleep(1000);

      browser.findElement(by.css('div[ui-view] button[ng-click="vm.gandalf.nextStep()"]')).then(function (element) {

        element.click().then(function () {

          browser.sleep(1000);

          browser.getCurrentUrl().then(function (url) {

            url.should.endsWith('/wiz/three/create/two');

            done();

          });

        });

      });

    });

  });

});
